const env = require("dotenv").config({})
const express = require("express")
const app = express()

app.use(express.json({ limit: "50mb" }))
app.get("/api", (req, res) => {
    res.send("Hello World")
})

app.get("/name", (req, res) => {
    res.send("")
})

app.delete("/name", (req, res) => {
    res.send("delete name")
})

app.post("/name", (req, res) => {
    res.send("get แบบ Post name")
})

app.put("/name", (req, res) => {
    res.send("Update name")
})

app.disable("x-powered-by")
app.listen(process.env.EXPRESS_PORT, async () => {
    console.log(`App listening on port ${process.env.EXPRESS_PORT}!`)
})
